# bankjco_server

1- Crear fichero de variables

".env" (touch .env)

2- A�adir las siguientes variables:

- MLAB_API_KEY= Key para acceso via http a MLAB
- JWT_SECRET= secreto para generar jwt
- PORT= puerto del servidor (por defecto 3000)
- MLAB_USER= usuario de conexion a Mlab
- MLAB_PWD= Contrase�a de conexion a Mlab
- MLAB_CONNECTION= cadena de conexion a Mlab
- LOGLEVEL= nivel de log (por defecto info)
