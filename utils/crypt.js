const bcrypt = require ('bcrypt');
var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
var jwt = require('jsonwebtoken');


function hash (data){
  winston.debug("Hashing data");
  //devuelve String
  if(data){
    return bcrypt.hashSync(data,10);
  }else{
    return ""
  }
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 winston.debug("Checking password");

 // Returns boolean
 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

function canOperate(token, userID) {
 winston.debug("canOperate");
 var dev=false;
 var decoded = jwt.verify(token, process.env.JWT_SECRET);
 if(decoded.rol=="A"){
   dev=true;
 }
 else if(decoded.id==userID){
   dev=true;
 }
 return dev;
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
module.exports.canOperate = canOperate;
