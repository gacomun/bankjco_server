var appRoot = require('app-root-path');
var winston = require('winston');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
// define the custom settings for each transport (file, console)
var options = {
  file: {
    level: process.env.LOGLEVEL || 'info',
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 1048576, // 1MB *1024*1024
    maxFiles: 5,
    colorize: false,
  },
  console: {
    level: process.env.LOGLEVEL || 'info',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

// instantiate a new Winston Logger with the settings defined above
var logger = createLogger({
  format: combine(
    timestamp(),
    myFormat
  ),
        transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ],
  exitOnError: false, // do not exit on handled exceptions
});

// // create a stream object with a 'write' function that will be used by `morgan`
// logger.stream = {
//   write: function(message, encoding) {
//     // use the 'info' log level so the output will be picked up by both transports (file and console)
//     logger.info(message);
//   },
// };

module.exports = logger;
