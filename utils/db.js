var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
var mongoose = require('mongoose');

mongoose.connect(`mongodb://${process.env.MLAB_USER}:${process.env.MLAB_PWD}@${process.env.MLAB_CONNECTION}`, {useNewUrlParser: true} );
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', winston.error.bind(console, 'MongoDB connection error:'));
