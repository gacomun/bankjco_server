use admin
db.createUser({
  user: "root",
  pwd: "root",
  roles:["root"],
  mechanisms:[
    "SCRAM-SHA-1"
  ]
});

use apitechujco12ed
db.createUser({
  user: "admin",
  pwd: "admin1234",
  "roles" : [
    {
      "role" : "readWrite",
      "db" : "apitechujco12ed"
    }
  ],
  mechanisms:[
    "SCRAM-SHA-1"
  ]
});
