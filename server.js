require('dotenv').config();
const express = require('express');//inclusión del modulo express
const app=express();// Inicializa el framework
var router = express.Router();
var cors = require('cors');
const winston = require('./utils/winston');
var jwt = require('jsonwebtoken');

// var enableCORS = function(req, res, next) {
//   res.set("Access-Control-Allow-Origin", "*");
//   res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
//
//   // This will be needed.
//   res.set("Access-Control-Allow-Headers", "Content-Type");
//
//   next();
// }

const userControllerV1 =require("./controllers/v1/UserController")
const accountControllerV1 =require("./controllers/v1/AccountController")
const sessionControllerV1 =require("./controllers/v1/SessionController")
const transactionControllerV1 =require("./controllers/v1/TransactionController")


const port = process.env.PORT || 3000; //por convecion las variables van en mayusculas
app.use(express.json());
// app.use(enableCORS);
router.all('*', cors());
// router.all('*', cors({
//   credentials: true,
//   exposedHeaders: ['X-tokensec']
// }));
app.use('/', router);
app.listen(port);
winston.info("\n----------------------------------------\nAPI escuchando en el puerto " + port);

// a middleware function with no mount path. This code is executed for every request to the router
router.use(function (req, res, next) {
  // var token = jwt.sign({ id: '123456',rol:"A" }, 'secret', { expiresIn: '999y' });
  // console.log(token);

  var MU=req.method+req.url;
  if(MU === "POST/bankjco/v1/users" || MU === "POST/bankjco/v1/sessions" || MU === "POST/bankjco/v1/users/" || MU === "POST/bankjco/v1/sessions/"){
      next();
  }else{
    var token=req.get('X-tokensec');
    if(token){
      // winston.debug(token);
      try {
        var decoded = jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
          if(err){
            winston.debug(err);
            res.status(msgEJWT.status);
            msgEJWT.dev.msg=err.message;
            res.send(msgEJWT.dev);
          }else{
            winston.debug(decoded);
            next();
          }
        });

      } catch (e) {
        res.status(msgEJWT.status);
        msgEJWT.dev.msg=err.message;
        res.send(msgEJWT.dev);
      } finally {

      }
      // verify a token symmetric - synchronous


    }else{
      res.status(msgEJWTNotExists.status);
      res.send(msgEJWTNotExists.dev);
    }
  }

});

router.get("/bankjco/v1/users/",userControllerV1.listUsers);
router.get("/bankjco/v1/users/:id",userControllerV1.getUser);
router.post("/bankjco/v1/users/",userControllerV1.postUser);
router.put("/bankjco/v1/users/:id",userControllerV1.putUser);
router.delete("/bankjco/v1/users/:id",userControllerV1.deleteUser);

router.get("/bankjco/v1/sessions/",sessionControllerV1.listSessions);
router.get("/bankjco/v1/sessions/:id",sessionControllerV1.getSession);
router.post("/bankjco/v1/sessions/",sessionControllerV1.postSession);
router.put("/bankjco/v1/sessions/:id",sessionControllerV1.putSession);
router.delete("/bankjco/v1/sessions/:id",sessionControllerV1.deleteSession);

router.get("/bankjco/v1/users/:idUser/accounts/",accountControllerV1.listAccounts);
router.get("/bankjco/v1/users/:idUser/accounts/:id",accountControllerV1.getAccount);
router.post("/bankjco/v1/users/:idUser/accounts/",accountControllerV1.postAccount);
router.put("/bankjco/v1/users/:idUser/accounts/:id",accountControllerV1.putAccount);
router.delete("/bankjco/v1/users/:idUser/accounts/:id",accountControllerV1.deleteAccount);


router.get("/bankjco/v1/users/:idUser/accounts/:idAccount/transactions/",transactionControllerV1.listTransactions);
router.get("/bankjco/v1/users/:idUser/accounts/:idAccount/transactions/:id",transactionControllerV1.getTransaction);
router.post("/bankjco/v1/users/:idUser/accounts/:idAccount/transactions/",transactionControllerV1.postTransaction);
router.put("/bankjco/v1/users/:idUser/accounts/:idAccount/transactions/:id",transactionControllerV1.putTransaction);
router.delete("/bankjco/v1/users/:idUser/accounts/:idAccount/transactions/:id",transactionControllerV1.deleteTransaction);
