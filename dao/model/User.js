var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    _id: {
      type: Number
    },
    first_name: String,
    middle_name:String,
    last_name :String,
    age : Number,
    email :{
      type: String,
      required : true
    },
    password :{
      type: String,
      // index: true,
      required : true
    },
    rol:String,
    logged:Boolean,
    created: {
        type: Date,
        default: Date.now
    }
});

var User = mongoose.model('User', userSchema);

module.exports = User;
