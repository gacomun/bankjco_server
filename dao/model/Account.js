var mongoose = require('mongoose');

var accountSchema = mongoose.Schema({
    _id: {
      type: Number
    },
    IBAN :{
      type: String,
      required : true
    },
    balance: {
        type: Number,
        default: 0
    },
    currency: {
        type: String,
        default: "EUR"
    },
    id_user : Number,
    created: {
        type: Date,
        default: Date.now
    }
});

var Account = mongoose.model('Account', accountSchema);

module.exports = Account;
