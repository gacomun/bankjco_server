var mongoose = require('mongoose');

var transactionSchema = mongoose.Schema({
    _id: {
      type: Number
    },
    concept: String,
    amount: {
        type: Number,
        required : true,
        default: 0
    },
    currency :{
        type: String,
        default: "EUR"
    },
    type : {
        type: String,
        required : true,
        enum: ['I', 'C']
    },
    account_id : Number,
    value_date: {
        type: Date,
        default: Date.now
    }
});

var Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;
