var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
require(appRoot.path+"/utils/db");
const accountModel=require(appRoot.path+"/dao/model/Account");
const crypt =require(appRoot.path+"/utils/crypt");


function listAccountsDao(req,res) {
  var metodo="listAccountsDao";
  winston.debug(metodo);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    var query = accountModel.find({id_user:req.params.idUser},{id_user:0}).exec(function(err, accounts) {
      if(err){
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (accounts != null) {
        var response= accounts;
        res.status(msgOGet.status);
        res.send(response);
      }else{
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    }
  );
}else{
  res.status(msgEJWTNotExists.status);
  res.send(msgEJWTNotExists.dev);
}
}

function getAccountDao(req,res) {
  var metodo="getAccountDao";
  winston.debug(`${metodo}(${req.params.idUser},${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    var query = accountModel.findOne({ '_id': req.params.id,'id_user': req.params.idUser},{id_user:0}).exec(function(err, account) {
      if(err){
        winston.error(`${metodo}: ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (account != null) {
        var response= account;
        res.status(msgOGet.status);
        res.send(response);
      }else {
        winston.error(`${metodo}: ${msgEUserNotExists}`);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    });
  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}

function postAccountDao(req,res) {
  var metodo="postAccountDao";
  winston.debug(metodo);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    accountModel.findOne({},{_id:1}).sort( { _id: -1 } ).exec(function(err, account) {
      if(err){
        winston.error(`${metodo}(busqueda id): ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else {
        var newAccount={
          "_id" : (account)?account._id+1:1,
          "IBAN" : req.body.IBAN,
          "balance" : req.body.balance,
          "currency" : req.body.currency,
          "id_user" : req.params.idUser
        }
        winston.debug(JSON.stringify(newAccount));
        accountModel.create(newAccount,
          function(errP, userP) {
            if(errP){
              winston.error(`${metodo}: ${errP.message}`);
              res.status(msgEBBDD.status);
              msgEBBDD.dev.msg=errP.message;
              res.send(msgEBBDD.dev);
            }else{
              delete newAccount.id_user;
              var response= newAccount;
              res.status(msgOPost.status);
              res.send(response);
            }
          }

        )
      }
    });
  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}

function putAccountDao(req,res) {

    var metodo="putAccountDao";
    winston.debug(`${metodo}(${req.params.idUser},${req.params.id})`);
    if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
      var query = accountModel.findOne({ '_id': req.params.id },{password:0,rol:0}).exec(function(err, account) {
        if(err){
          winston.error(`${metodo}(busqueda id): ${err.message}`);
          res.status(msgEBBDD.status);
          msgEBBDD.dev.msg=err.message;
          res.send(msgEBBDD.dev);
        }else if (account != null) {
          var newAccount={
            "IBAN" : req.body.IBAN,
            "balance" : req.body.balance,
            "currency" : req.body.currency,
            "id_user" : req.params.idUser
          }
          accountModel.findOneAndUpdate({"_id":req.params.id},newAccount,function(errM, userM) {
            if(errM){
              winston.error(`${metodo}: ${errM.message}`);
              res.status(msgEBBDD.status);
              msgEBBDD.dev.msg=errM.message;
              res.send(msgEBBDD.dev);
            }else {
              delete newAccount.id_user
              var response= newAccount;
              res.status(msgOPut.status);
              res.send(response);
            }
          });
        }else{
          winston.error(`${metodo} (busqueda id): ${msgEUserNotExists.dev}`);
          res.status(msgEUserNotExists.status);
          res.send(msgEUserNotExists.dev);
        }
      }
    );
  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}
function deleteAccountDao(req,res) {
  var metodo="deleteAccountDao";
  winston.debug(`${metodo}(${req.params.idUser},${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    var query = accountModel.findOne({ '_id': req.params.id },{_id:1}).exec(function(err, account) {
      if(err){
        winston.error(`${metodo} (busqueda id): ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (account != null) {
        accountModel.findOneAndDelete({"_id":req.params.id},function(errD, accountD) {
          if(errD){
            winston.error(`${metodo}: ${errD.message}`);
            res.status(msgEBBDD.status);
            msgEBBDD.dev.msg=errD.message;
            res.send(msgEBBDD.dev);
          }else {
            res.status(msgODelete.status);
            res.send(msgODelete.dev);
          }
        });
      }else {
        winston.error(`${metodo} (busqueda id): ${msgEUserNotExists.dev}`);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    });

  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}

module.exports.listAccountsDao = listAccountsDao;
module.exports.getAccountDao = getAccountDao;
module.exports.postAccountDao = postAccountDao;
module.exports.putAccountDao = putAccountDao;
module.exports.deleteAccountDao = deleteAccountDao;
