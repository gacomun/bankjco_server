var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
const userModel=require(appRoot.path+"/dao/model/User");
var jwt = require('jsonwebtoken');
const crypt =require(appRoot.path+"/utils/crypt");

function listSessionsDao() {
  winston.debug("listSessionsDao");

}

function getSessionDao() {
  winston.debug(`getSessionDao(${appRoot})`);
}

function postSessionDao(req,res) {
  var metodo="postSessionDao";
  winston.debug(metodo);
  userModel.findOne({ 'email': req.body.email }).exec(function(err, user) {
    if(err){
      winston.error(`${metodo}(busqueda): ${err.message}`);
      res.status(msgEBBDD.status);
      msgEBBDD.dev.msg=err.message;
      res.send(msgEBBDD.dev);
    }else if (user != null && crypt.checkPassword(req.body.password,user.password)) {
      var token = jwt.sign({id:user._id,rol:user.rol}, process.env.JWT_SECRET, { expiresIn: '1h' });
      var newUser={
        "first_name" :user.first_name,
        "middle_name":user.middle_name,
        "last_name" :user.last_name,
        "age" :user.age,
        "email" :user.email,
        "password" :user.password,
        "rol":(user.rol)?user.rol:"U",
        "logged":true
      }
      userModel.findOneAndUpdate({"_id":user._id},newUser,function(errM, userM) {
        if(errM){
          winston.error(`postSessionDao: ${errM.message}`);
          res.status(msgEBBDD.status);
          msgEBBDD.dev.msg=errM.message;
          res.send(msgEBBDD.dev);
        }else {
          var newUser={
            _id:userM._id,
            X_tokensec:token
          };

          var response= newUser;
          res.status(msgOPost.status);
          res.set('X-tokensec', token);
          res.send(response);
        }
      });
    }else {
      winston.error("postSessionDao: "+ msgEUserNotExists.dev);
      res.status(msgEUserNotExists.status);
      res.send(msgEUserNotExists.dev);
    }
  }
);
}




function putSessionDao(id) {
  winston.debug(`putSessionDao(${appRoot})`);
}
function deleteSessionDao(req,res) {
  var metodo="deleteSessionDao";
  winston.debug(`${metodo}(${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.id)){
    userModel.findOne({ '_id': req.params.id }).exec(function(err, user) {
      if(err){
        winston.error(`${metodo}(busqueda): ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (user != null) {
        var newUser={
          "first_name" :user.first_name,
          "middle_name":user.middle_name,
          "last_name" :user.last_name,
          "age" :user.age,
          "email" :user.email,
          "password" :user.password,
          "rol":(user.rol)?user.rol:"U",
        }
        userModel.findOneAndUpdate({"_id":req.params.id},newUser,function(errM, userM) {
          if(errM){
            winston.error(`${metodo}: ${errM.message}`);
            res.status(msgEBBDD.status);
            msgEBBDD.dev.msg=errM.message;
            res.send(msgEBBDD.dev);
          }else {
            res.status(msgODelete.status);
            res.send(msgODelete.dev);
          }
        });
      }else {
        winston.error("postSessionDao: "+ msgEUserNotExists.dev);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    }
  );
}else{
  res.status(msgEJWTNotExists.status);
  res.send(msgEJWTNotExists.dev);
}
}

module.exports.postSessionDao = postSessionDao;
module.exports.deleteSessionDao = deleteSessionDao;
