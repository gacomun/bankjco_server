var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
require(appRoot.path+"/utils/db");
const transactionModel=require(appRoot.path+"/dao/model/Transaction");
const accountModel=require(appRoot.path+"/dao/model/Account");
const crypt =require(appRoot.path+"/utils/crypt");

function listTransactionsDao(req,res) {
  var metodo="listTransactionsDao";
  winston.debug(metodo);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    var query = transactionModel.find({account_id:req.params.idAccount},{account_id:0}).exec(function(err, transactions) {
      if(err){
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (transactions != null) {
        var response= transactions;
        res.status(msgOGet.status);
        res.send(response);
      }else{
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    }
  );
}else{
  res.status(msgEJWTNotExists.status);
  res.send(msgEJWTNotExists.dev);
}
}

function getTransactionDao(req,res) {
  var metodo="getTransactionDao";
  winston.debug(`${metodo}(${req.params.idUser},${req.params.idAccount},${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    var query = transactionModel.findOne({ '_id': req.params.id,'account_id': req.params.idAccount},{account_id:0}).exec(function(err, transaction) {
      if(err){
        winston.error(`${metodo}: ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (transaction != null) {
        var response= transaction;
        res.status(msgOGet.status);
        res.send(response);
      }else {
        winston.error(`${metodo}: ${msgEUserNotExists}`);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    });
  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}

function postTransactionDao(req,res) {
  var metodo="postTransactionDao";
  winston.debug(metodo);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
    transactionModel.findOne({},{_id:1}).sort( { _id: -1 } ).exec(function(err, transaction) {
      if(err){
        winston.error(`${metodo}(busqueda id): ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else {
        var newTransaction={
          "_id": (transaction)?transaction._id+1:1,
          "concept": req.body.concept,
          "amount": req.body.amount,
          "currency": req.body.currency,
          "value_date": (req.body.value_date)?req.body.value_date:new Date(),
          "type":req.body.type,
          "account_id": req.params.idAccount
        }
        winston.debug(JSON.stringify(newTransaction));
        transactionModel.create(newTransaction,
          function(errP, userP) {
            if(errP){
              winston.error(`${metodo}: ${errP.message}`);
              res.status(msgEBBDD.status);
              msgEBBDD.dev.msg=errP.message;
              res.send(msgEBBDD.dev);
            }else{
              var bal=(newTransaction.type==="C")?(-1*req.body.amount):req.body.amount
              accountModel.findOneAndUpdate({"_id":req.params.idAccount},{ $inc: { balance: bal } },function(errA, accountA) {
                console.log(errA);
                console.log(accountA);
                if(errA){
                  winston.error(`${metodo}: ${errA.message}`);
                  res.status(msgEBBDD.status);
                  msgEBBDD.dev.msg=errA.message;
                  res.send(msgEBBDD.dev);
                }else {
                  delete newTransaction.account_id;
                  var response= newTransaction;
                  res.status(msgOPost.status);
                  res.send(response);
                }
              });
            }
          }

        )
      }
    });
  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}

function putTransactionDao(req,res) {

      var metodo="putTransactionDao";
      winston.debug(`${metodo}(${req.params.idUser},${req.params.idAccount},${req.params.id})`);
      if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
        var query = transactionModel.findOne({ '_id': req.params.id,"account_id": req.params.idAccount},{}).exec(function(err, transaction) {
          if(err){
            winston.error(`${metodo}(busqueda id): ${err.message}`);
            res.status(msgEBBDD.status);
            msgEBBDD.dev.msg=err.message;
            res.send(msgEBBDD.dev);
          }else if (transaction != null) {
            var newTransaction={
              "concept": req.body.concept,
              "amount": req.body.amount,
              "currency": req.body.currency,
              "value_date": (req.body.value_date)?req.body.value_date:new Date(),
              "type":req.body.type,
              "account_id": req.params.idAccount
            }
            transactionModel.findOneAndUpdate({"_id":req.params.id},newTransaction,function(errM, transactionM) {
              if(errM){
                winston.error(`${metodo}: ${errM.message}`);
                res.status(msgEBBDD.status);
                msgEBBDD.dev.msg=errM.message;
                res.send(msgEBBDD.dev);
              }else {
                var bal=(newTransaction.type==="C")?(-1*req.body.amount):req.body.amount
                accountModel.findOneAndUpdate({"_id":req.params.idAccount},{ $inc: { balance: bal } },function(errA, accountA) {
                  console.log(errA);
                  console.log(accountA);
                  if(errA){
                    winston.error(`${metodo}: ${errA.message}`);
                    res.status(msgEBBDD.status);
                    msgEBBDD.dev.msg=errA.message;
                    res.send(msgEBBDD.dev);
                  }else {
                    delete newTransaction.account_id
                    var response= newTransaction;
                    res.status(msgOPut.status);
                    res.send(response);
                  }
                });
              }
            });
          }else{
            winston.error(`${metodo} (busqueda id): ${msgEUserNotExists.dev}`);
            res.status(msgEUserNotExists.status);
            res.send(msgEUserNotExists.dev);
          }
        }
      );
    }else{
      res.status(msgEJWTNotExists.status);
      res.send(msgEJWTNotExists.dev);
    }
}
function deleteTransactionDao(req,res) {

    var metodo="deleteTransactionDao";
    winston.debug(`${metodo}(${req.params.idUser},${req.params.idAccount},${req.params.id})`);
    if(crypt.canOperate(req.get('X-tokensec'),req.params.idUser)){
      var query = transactionModel.findOne({ '_id': req.params.id,"account_id": req.params.idAccount },{_id:1}).exec(function(err, transaction) {
        if(err){
          winston.error(`${metodo} (busqueda id): ${err.message}`);
          res.status(msgEBBDD.status);
          msgEBBDD.dev.msg=err.message;
          res.send(msgEBBDD.dev);
        }else if (transaction != null) {
          transactionModel.findOneAndDelete({"_id":req.params.id},function(errD, transactionD) {
            if(errD){
              winston.error(`${metodo}: ${errD.message}`);
              res.status(msgEBBDD.status);
              msgEBBDD.dev.msg=errD.message;
              res.send(msgEBBDD.dev);
            }else {
              res.status(msgODelete.status);
              res.send(msgODelete.dev);
            }
          });
        }else {
          winston.error(`${metodo} (busqueda id): ${msgEUserNotExists.dev}`);
          res.status(msgEUserNotExists.status);
          res.send(msgEUserNotExists.dev);
        }
      });

    }else{
      res.status(msgEJWTNotExists.status);
      res.send(msgEJWTNotExists.dev);
    }
}

module.exports.listTransactionsDao = listTransactionsDao;
module.exports.getTransactionDao = getTransactionDao;
module.exports.postTransactionDao = postTransactionDao;
module.exports.putTransactionDao = putTransactionDao;
module.exports.deleteTransactionDao = deleteTransactionDao;
