var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
const crypt =require(appRoot.path+"/utils/crypt");
require(appRoot.path+'/message/error');
const requestJson=require("request-json");
require(appRoot.path+'/utils/db');
const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;
require(appRoot.path+"/utils/db");
const userModel=require(appRoot.path+"/dao/model/User");


function listUsersDao(req,res) {
  var metodo="listUsersDao";
  winston.debug(metodo);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.id)){
    var query = userModel.find({},{password:0,rol:0}).exec(function(err, users) {
      if(err){
        winston.error(`${metodo}: ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (users != null) {
        var response= users;
        res.status(msgOGet.status);
        res.send(response);
      }else{
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    }
  );
}else{
  res.status(msgEJWTNotExists.status);
  res.send(msgEJWTNotExists.dev);
}
}


function getUserDao(req,res) {
  var metodo="getUserDao";
  winston.debug(`${metodo}(${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.id)){
    var query = userModel.findOne({ '_id': req.params.id },{password:0,rol:0}).exec(function(err, user) {
      if(err){
        winston.error(`${metodo}: ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (user != null) {
        var response= user;
        res.status(msgOGet.status);
        res.send(response);
      }else {
        winston.error(`${metodo}: ${msgEUserNotExists}`);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    });
  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }
}

function postUserDao(req,res) {
  var metodo="postUserDao";
  winston.debug(metodo);
  userModel.findOne({ 'email': req.body.email }).exec(function(errV, userV) {
    if(errV){
      winston.error(`${metodo}: ${errV.message}`);
      res.status(msgEBBDD.status);
      msgEBBDD.dev.msg=errV.message;
      res.send(msgEBBDD.dev);
    }else if (userV != null) {
      winston.error(`${metodo}: ${msgEUserExists.dev}`);
      res.status(msgEUserExists.status);
      res.send(msgEUserExists.dev);
    }else {
      userModel.findOne({},{_id:1}).sort( { _id: -1 } ).exec(function(err, user) {
        if(err){
          winston.error(`${metodo}(busqueda id): ${err.message}`);
          res.status(msgEBBDD.status);
          msgEBBDD.dev.msg=err.message;
          res.send(msgEBBDD.dev);
        }else {
          var newUser={
            "_id" :(user)?user._id+1:1,
            "first_name" :req.body.first_name,
            "middle_name":req.body.middle_name,
            "last_name" :req.body.last_name,
            "age" :req.body.age,
            "email" :req.body.email,
            "password" :crypt.hash(req.body.password),
            "rol":(req.body.rol)?req.body.rol:"U"
          }
          winston.debug(JSON.stringify(newUser));
          userModel.create(newUser,
            function(errP, userP) {
              if(errP){
                winston.error(`${metodo}: ${errP.message}`);
                res.status(msgEBBDD.status);
                msgEBBDD.dev.msg=errP.message;
                res.send(msgEBBDD.dev);
              }else{
                delete newUser.password
                delete newUser.rol
                var response= newUser;
                res.status(msgOPost.status);
                res.send(response);
              }
            }

          )
        }
      });
    }
  });



}

function putUserDao(req,res) {
  var metodo="putUserDao";
  winston.debug(`${metodo}(${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.id)){
    var query = userModel.findOne({ '_id': req.params.id ,"email":req.body.email},{}).exec(function(err, user) {
      if(err){
        winston.error(`${metodo}(busqueda id): ${err.message}`);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (user != null) {
        var newUser={
          "first_name" :req.body.first_name,
          "middle_name":req.body.middle_name,
          "last_name" :req.body.last_name,
          "age" :req.body.age,
          "email" :req.body.email,
          "password" :user.password,
          "rol":user.rol
        }
        userModel.findOneAndUpdate({"_id":req.params.id},newUser,function(errM, userM) {
          if(errM){
            winston.error(`${metodo}: ${errM.message}`);
            res.status(msgEBBDD.status);
            msgEBBDD.dev.msg=errM.message;
            res.send(msgEBBDD.dev);
          }else {
            delete newUser.password
            delete newUser.rol
            var response= newUser;
            res.status(msgOPut.status);
            res.send(response);
          }
        });
      }else{
        winston.error(`${metodo} (busqueda id): ${msgEUserNotExists.dev}`);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    }
  );
}else{
  res.status(msgEJWTNotExists.status);
  res.send(msgEJWTNotExists.dev);
}
}

function deleteUserDao(req,res) {
  var metodo="deleteUserDao";
  winston.debug(`${metodo}(${req.params.id})`);
  if(crypt.canOperate(req.get('X-tokensec'),req.params.id)){
    var query = userModel.findOne({ '_id': req.params.id },{_id:1}).exec(function(err, user) {
      if(err){
        winston.error("deleteUserDao (busqueda id): "+ err.message);
        res.status(msgEBBDD.status);
        msgEBBDD.dev.msg=err.message;
        res.send(msgEBBDD.dev);
      }else if (user != null) {
        console.log(req.params.id);
        userModel.findOneAndDelete({"_id":req.params.id},function(errD, userD) {
          if(errD){
            winston.error(`${metodo} (busqueda id): ${err.message}`);
            res.status(msgEBBDD.status);
            msgEBBDD.dev.msg=errD.message;
            res.send(msgEBBDD.dev);
          }else {
            res.status(msgODelete.status);
            res.send(msgODelete.dev);
          }
        });
      }else {
        winston.error(`${metodo} (busqueda id): ${msgEUserNotExists.dev}`);
        res.status(msgEUserNotExists.status);
        res.send(msgEUserNotExists.dev);
      }
    });

  }else{
    res.status(msgEJWTNotExists.status);
    res.send(msgEJWTNotExists.dev);
  }

}



module.exports.listUsersDao = listUsersDao;
module.exports.getUserDao = getUserDao;
module.exports.postUserDao = postUserDao;
module.exports.putUserDao = putUserDao;
module.exports.deleteUserDao = deleteUserDao;
