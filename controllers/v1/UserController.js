var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
require(appRoot.path+'/message/error');

const usersDao = require(appRoot.path+'/dao/v1/UsersDao');




function listUsers(req,res) {
  winston.info("GET /bankjco/v1/users");
  usersDao.listUsersDao(req,res);
}



function getUser(req,res) {
  winston.info("GET /bankjco/v1/users/"+req.params.id);
  var dev=usersDao.getUserDao(req,res);
}

function postUser(req,res) {
  winston.info("POST /bankjco/v1/users/");
  usersDao.postUserDao(req,res);
}

function putUser(req,res) {
  winston.info("PUT /bankjco/v1/users/"+req.params.id);
  usersDao.putUserDao(req,res);

}
function deleteUser(req,res) {
  winston.info("DELETE /bankjco/v1/users/"+req.params.id);
  usersDao.deleteUserDao(req,res);

}



module.exports.listUsers = listUsers;
module.exports.getUser = getUser;
module.exports.postUser = postUser;
module.exports.putUser = putUser;
module.exports.deleteUser = deleteUser;
