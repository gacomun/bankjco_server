var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
require(appRoot.path+'/message/error');

const accountsDao = require(appRoot.path+'/dao/v1/AccountsDao');

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;


function listAccounts(req,res) {
  winston.info(`GET /bankjco/v1/users/${req.params.idUser}/accounts`);
  accountsDao.listAccountsDao(req,res);
}

function getAccount(req,res) {
  winston.info(`GET /bankjco/v1/users/${req.params.idUser}/accounts/${req.params.id}`);
  accountsDao.getAccountDao(req,res);
}

function postAccount(req,res) {
  winston.info(`POST /bankjco/v1/users/${req.params.idUser}/accounts/`);
  accountsDao.postAccountDao(req,res);
}

function putAccount(req,res) {
  winston.info(`PUT /bankjco/v1/users/${req.params.idUser}/accounts/${req.params.id}`);
  accountsDao.putAccountDao(req,res);
}

function deleteAccount(req,res) {
  winston.info(`DELETE /bankjco/v1/users/${req.params.idUser}/accounts/${req.params.id}`);
  accountsDao.deleteAccountDao(req,res);
}

module.exports.listAccounts = listAccounts;
module.exports.getAccount = getAccount;
module.exports.postAccount = postAccount;
module.exports.putAccount = putAccount;
module.exports.deleteAccount = deleteAccount;
