var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
require(appRoot.path+'/message/error');

const sessionsDao = require(appRoot.path+'/dao/v1/SessionsDao');

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;


function listSessions(req,res) {
  winston.info("GET /bankjco/v1/sessions");
  res.status(msgENotImplemented.status);
  res.send(msgENotImplemented.dev);
}

function getSession(req,res) {
  winston.info("GET /bankjco/v1/sessions/"+req.params.id);
  res.status(msgENotImplemented.status);
  res.send(msgENotImplemented.dev);
}

function postSession(req,res) {
  winston.info("POST /bankjco/v1/sessions/");
  sessionsDao.postSessionDao(req,res);
}

function putSession(req,res) {
  winston.info("PUT /bankjco/v1/sessions/"+req.params.id);
  res.status(msgENotImplemented.status);
  res.send(msgENotImplemented.dev);
}
function deleteSession(req,res) {
  winston.info("DELETE /bankjco/v1/sessions/"+req.params.id);
  sessionsDao.deleteSessionDao(req,res);
}

module.exports.listSessions = listSessions;
module.exports.getSession = getSession;
module.exports.postSession = postSession;
module.exports.putSession = putSession;
module.exports.deleteSession = deleteSession;
