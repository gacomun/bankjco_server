var appRoot = require('app-root-path');
const winston = require(appRoot.path+'/utils/winston');
require(appRoot.path+'/message/error');

const transactionsDao = require(appRoot.path+'/dao/v1/TransactionsDao');

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechujco12ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;


function listTransactions(req,res) {
  winston.info(`GET /bankjco/v1/${req.params.idUser}/accounts/${req.params.idAccount}/transactions`);
  transactionsDao.listTransactionsDao(req,res);
}

function getTransaction(req,res) {
  winston.info(`GET /bankjco/v1/${req.params.idUser}/accounts/${req.params.idAccount}/transactions/${req.params.id}`);
  transactionsDao.getTransactionDao(req,res);
}

function postTransaction(req,res) {
  winston.info(`POST /bankjco/v1/${req.params.idUser}/accounts/${req.params.idAccount}/transactions`);
  transactionsDao.postTransactionDao(req,res);
}

function putTransaction(req,res) {
  winston.info(`PUT /bankjco/v1/${req.params.idUser}/accounts/${req.params.idAccount}/transactions/${req.params.id}`);
  transactionsDao.putTransactionDao(req,res);
}
function deleteTransaction(req,res) {
  winston.info(`DELETE /bankjco/v1/${req.params.idUser}/accounts/${req.params.idAccount}/transactions/${req.params.id}`);
  transactionsDao.deleteTransactionDao(req,res);
}
module.exports.listTransactions = listTransactions;
module.exports.getTransaction = getTransaction;
module.exports.postTransaction = postTransaction;
module.exports.putTransaction = putTransaction;
module.exports.deleteTransaction = deleteTransaction;
