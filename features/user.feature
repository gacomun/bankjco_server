Feature: Crud User

  Revisar que el Crud de Usuarios está operativo

  Scenario: listar todos los usuarios
    Given un token valido
    Then obtengo una lista de usuarios

  Scenario Outline: Detalle Usuario con id incorrecto
    Given un "<id>" usuario incorrecto
    Given un token valido
    Then obtener un "<statusCode>" en detalle

 Examples:
     | id            | statusCode |
     | 999999        | 404        |
     | a             | 500        |

  Scenario: Detalle Usuario con id correcto
    Given un id usuario correcto
    Given un token valido
    Then obtener detalle de usuario


#  Scenario: Alta Usuario con datos correctos
#    Given un usuario correcto
#    Then obtener alta de usuario

#  Scenario Outline: Alta Usuario con datos incorrectos
#    Given un "<usuario>" incorrecto
#    Given un token valido
#    Then obtener "<statusCode>" alta

#    Examples:
#      | usuario       | statusCode |
#      | 1             | 400        |
#      | 2             | 400        |
#      | 3             | 400        |

#  Scenario Outline: Modificacion Usuario incorrecto
#    Given un "<usuario>" incorrecto
#    Given un token valido
#    Then obtener "<statusCode>" en modificacion

#    Examples:
#      | usuario       | statusCode |
#      | 1             | 404        |
#      | 2             | 500        |
#      | 3             | 404        |

#  Scenario: Modificacion Usuario con datos correctos
#    Given un usuario correcto modificacion
#    Given un token valido
#    Then obtener modificacion de usuario

#  Scenario Outline: Baja Usuario con id incorrecto
#    Given un "<id>" usuario incorrecto
#    Given un token valido
#    Then obtener un "<statusCode>" en baja

#    Examples:
#        | id            | statusCode |
#        | 999999        | 404        |
#        | a             | 500        |


#  Scenario: Baja Usuario con id correcto
#    Given un token valido
#    Then obtener baja de usuario
