Feature: Crud Session

  Revisar que el Crud de Sesiones está operativo

  Scenario: Alta Session con datos correctos
    Given una sesionId correcta
    Then obtener session de usuario

  Scenario Outline: Alta session con datos incorrectos
    Given un "<id>" incorrecto para alta sesion
    Then obtener "<statusCode>" alta session

    Examples:
      | sessionId       | statusCode |
      | 1               | 404        |
      | 2               | 404        |


  Scenario Outline: Baja Sesion con id incorrecto
    Given un "<id>" session incorrecto
    Given un token valido
    Then obtener un "<statusCode>" en baja session

    Examples:
        | id            | statusCode |
        | 999999        | 404        |
        | a             | 500        |
        | 27            | 204        |
