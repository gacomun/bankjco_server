Feature: Crud Transaction

  Revisar que el Crud de Movimientos está operativo

  Scenario: Transaction listar todos
    Given un token valido
    Then Transaction obtengo una lista

  Scenario Outline: Transaction detalle incorrecto
    Given Transaction un "<id>", un "<idUser>", un "<idAccount>" incorrecto
    Given un token valido
    Then Transaction obtener un "<statusCode>" en detalle

      Examples:
        | id     | idUser | idAccount | statusCode |
        | 999999 | 999999 | 999999    | 404        |
        | 32     | 999999 | 999999    | 404        |
        | 999999 | 29     | 999999    | 404        |
        | 999999 | 999999 | 54        | 404        |
        | 32     | 29     | 999999    | 404        |
        | 999999 | 29     | 54        | 404        |
        | 32     | 999999 | 54        | 404        |
        | a      | a      | a         | 500        |
        | 32     | a      | a         | 500        |
        | a      | 29     | a         | 500        |
        | a      | a      | 54        | 500        |
        | 32     | 29     | a         | 500        |
        | a      | 29     | 54        | 500        |
        | 32     | a      | 54        | 404        |

  Scenario: Transaction Detalle con ids correcto
    Given Transaction ids detalle correctos
    Given un token valido
    Then Transaction obtener detalle de cuenta


#  Scenario: Alta Usuario con datos correctos
#    Given un usuario correcto
#    Then obtener alta de usuario

#  Scenario Outline: Alta Usuario con datos incorrectos
#    Given un "<usuario>" incorrecto
#    Given un token valido
#    Then obtener "<statusCode>" alta

#    Examples:
#      | usuario       | statusCode |
#      | 1             | 400        |
#      | 2             | 400        |
#      | 3             | 400        |

#  Scenario Outline: Modificacion Usuario incorrecto
#    Given un "<usuario>" incorrecto
#    Given un token valido
#    Then obtener "<statusCode>" en modificacion

#    Examples:
#      | usuario       | statusCode |
#      | 1             | 404        |
#      | 2             | 500        |
#      | 3             | 404        |

#  Scenario: Modificacion Usuario con datos correctos
#    Given un usuario correcto modificacion
#    Given un token valido
#    Then obtener modificacion de usuario

#  Scenario Outline: Baja Usuario con id incorrecto
#    Given un "<id>" usuario incorrecto
#    Given un token valido
#    Then obtener un "<statusCode>" en baja

#    Examples:
#        | id            | statusCode |
#        | 999999        | 404        |
#        | a             | 500        |


#  Scenario: Baja Usuario con id correcto
#    Given un token valido
#    Then obtener baja de usuario
