Feature: Crud Account

  Revisar que el Crud de Cuentas está operativo

  Scenario: Account listar todos
    Given un token valido
    Then Account obtengo una lista

  Scenario Outline: Account detalle incorrecto
    Given Account un "<id>", un "<idUser>" incorrecto
    Given un token valido
    Then Account obtener un "<statusCode>" en detalle

      Examples:
        | id     | idUser      | statusCode |
        | 999999 | 999999      | 404        |
        | 32     | 999999      | 404        |
        | 999999 | 29          | 404        |
        | a      | a           | 500        |
        | 32     | a           | 500        |
        | a      | 29          | 500        |

  Scenario: Account Detalle con ids correcto
    Given Account ids detalle correctos
    Given un token valido
    Then Account obtener detalle de cuenta


#  Scenario: Alta Usuario con datos correctos
#    Given un usuario correcto
#    Then obtener alta de usuario

#  Scenario Outline: Alta Usuario con datos incorrectos
#    Given un "<usuario>" incorrecto
#    Given un token valido
#    Then obtener "<statusCode>" alta

#    Examples:
#      | usuario       | statusCode |
#      | 1             | 400        |
#      | 2             | 400        |
#      | 3             | 400        |

#  Scenario Outline: Modificacion Usuario incorrecto
#    Given un "<usuario>" incorrecto
#    Given un token valido
#    Then obtener "<statusCode>" en modificacion

#    Examples:
#      | usuario       | statusCode |
#      | 1             | 404        |
#      | 2             | 500        |
#      | 3             | 404        |

#  Scenario: Modificacion Usuario con datos correctos
#    Given un usuario correcto modificacion
#    Given un token valido
#    Then obtener modificacion de usuario

#  Scenario Outline: Baja Usuario con id incorrecto
#    Given un "<id>" usuario incorrecto
#    Given un token valido
#    Then obtener un "<statusCode>" en baja

#    Examples:
#        | id            | statusCode |
#        | 999999        | 404        |
#        | a             | 500        |


#  Scenario: Baja Usuario con id correcto
#    Given un token valido
#    Then obtener baja de usuario
