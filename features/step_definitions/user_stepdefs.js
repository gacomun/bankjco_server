const assert = require('assert');
const { Given, When, Then, setDefaultTimeout } = require('cucumber');

const chai = require ('chai');
const chaihttp = require ('chai-http');

setDefaultTimeout(60 * 1000);
chai.use(chaihttp);


Then('obtengo una lista de usuarios', function () {
  chai.request('http://localhost:3000')
  .get('/bankjco/v1/users')
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.body.length > 0, true);
    }
  )
});

Given('un {string} usuario incorrecto', function (string) {
  this.idUser=string;
});
Then('obtener un {string} en detalle', function (string,done) {
  var user=this.idUser;
  chai.request('http://localhost:3000')
  .get('/bankjco/v1/users/'+user)
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,string);
      assert(res.body.msg);
      assert(res.body.code);
      done();
    }
  )
});
Given('un id usuario correcto', function () {
  this.idUser=1;
 });
 Then('obtener detalle de usuario', function (done) {
   var user=this.idUser;
   chai.request('http://localhost:3000')
   .get('/bankjco/v1/users/'+user)
   .set("X-tokensec",this.xtoken)
   .end(
     function(err,res) {
       assert.equal(res.statusCode,200);
       assert(res.body);
       assert(res.body._id);
       assert(res.body.email);
       done();
     }
   )
 });
Given('un usuario correcto', function () {
  var newUser={
    "first_name" :"Javier",
    "middle_name":"Cañada",
    "last_name" :"Ojeda",
    "age" :39,
    "email" :"j@bbva.com",
    "password" :"123456"
  }
  this.user=newUser;
});
Then('obtener alta de usuario', function (done) {
  var newUser=this.user;
  //ALta Usuario
  delete newUser._id;
  chai.request('http://localhost:3000')
  .post('/bankjco/v1/users')
  .send(newUser)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,201);
      assert(res.body);
      assert(res.body._id);
      done();
    }
  )
});
Given('un {string} incorrecto', function (string) {
  if(string=="1"){
    var newUser= {
          "_id": 1,
          "first_name": "Javier",
          "middle_name": "Cañada",
          "last_name": "Ojeda",
          "email": "j@bbva.com",
          "age": "a",
          "password": "123456",
          "rol":"A"
      };
    this.user=newUser;
  }else if (string=="2") {
    var newUser= {
          "_id": "a",
          "first_name": "Javier",
          "middle_name": "Cañada",
          "last_name": "Ojeda",
          "email": "j@bbva.com",
          "age": 39,
          "password": "123456",
          "rol":"A"
      };
    this.user=newUser;
  }else if (string=="3") {
    var newUser= {
          "_id": 1,
          "first_name": "Javier",
          "middle_name": "Cañada",
          "last_name": "Ojeda",
          "email": "j@bbva.com",
          "age": 39,
          "password": "123456",
          "rol":"A"
      };
    this.user=newUser;
    }

});
Then('obtener {string} alta', function (string,done) {
  var newUser=this.user;
  //ALta Usuario
  delete newUser._id;
  chai.request('http://localhost:3000')
  .post('/bankjco/v1/users')
  .set("X-tokensec",this.xtoken)
  .send(newUser)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,string);
      assert(res.body.msg);
      assert(res.body.code);
      done();
    }
  )
  });

Then('obtener {string} en modificacion', function (string,done) {
  var newUser=this.user;
  var user=newUser._id;
  delete newUser._id;
  chai.request('http://localhost:3000')
  .put('/bankjco/v1/users/'+user)
  .set("X-tokensec",this.xtoken)
  .send(newUser)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,string);
      assert(res.body.msg);
      assert(res.body.code);
      done();
    }
  )
  });

Given('un usuario correcto modificacion', function () {
  var newUser={
    "_id":1,
    "first_name":"Cristi",
    "middle_name":"Broseke",
    "last_name":"Pedrazzi",
    "age":136,
    "email":"cpedrazzi0@hhs.gov",
    "password":"123456",
    "rol":"A"
  };
  this.user=newUser;
});

Then('obtener modificacion de usuario', function (done) {
  // Modificacion Usuario
  var newUser=this.user;
  var idUser=newUser._id;
  delete newUser._id;
  chai.request('http://localhost:3000')
  .put('/bankjco/v1/users/'+idUser)
  .set("X-tokensec",this.xtoken)
  .send(newUser)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,200);
      assert(res.body);
      done();
    }
  )
});
Then('obtener baja de usuario', function (done) {
  var xtoken=this.xtoken;
  chai.request('http://localhost:3000')
  .get('/bankjco/v1/users')
  .set("X-tokensec",xtoken)
  .end(
    function(err,res) {
      idUser=(res.body[res.body.length-1])._id;
      // Borrado Usuario
      chai.request('http://localhost:3000')
      .delete('/bankjco/v1/users/'+idUser)
      .set("X-tokensec",xtoken)
      .end(
        function(errB,resB) {
          assert.equal(resB.statusCode,204);
          assert.equal(JSON.stringify(resB.body),"{}");
          done();
        }
      )
    }
  )
});

Then('obtener un {string} en baja', function (string,done) {
  var user=this.idUser;
      // Borrado Usuario
      chai.request('http://localhost:3000')
      .delete('/bankjco/v1/users/'+user)
      .set("X-tokensec",this.xtoken)
      .end(
        function(err,res) {
          assert.equal(res.statusCode,string);
          assert(res.body.msg);
          assert(res.body.code);
          done();
        }
      )

 });
