const assert = require('assert');
const { Given, When, Then, setDefaultTimeout } = require('cucumber');

const chai = require ('chai');
const chaihttp = require ('chai-http');

setDefaultTimeout(60 * 1000);
chai.use(chaihttp);

Given('un token valido', function () {
  this.xtoken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1NiIsInJvbCI6IkEiLCJpYXQiOjE1NTU3NTExOTYsImV4cCI6MzMwODE3OTM1OTZ9.Mj5in1dRG7SHP3l_tDX3HWRciBrLTMg6Rnmzt_ji9TU";
});
Given('un token invalido', function () {
  this.xtoken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1NiIsInJvbCI6IkEiLCJpYXQiOjE1NTU3NTExMjMsImV4cCI6MTU1NTc1MTEyNH0.SmYuxBixHvSfDHOIAfwOSAUrzfioepjo-QsJ7rct8tw";
});
Given('una sesionId correcta', function () {
  var newSession= {
      "email": "dblaslq@elpais.com",
      "password": "123456"
  }
  this.session=newSession;
});
Then('obtener session de usuario', function (done) {
  var newSession=this.session;
  chai.request('http://localhost:3000')
  .post('/bankjco/v1/sessions')
  .send(newSession)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,201);
      assert(res.body);
      assert(res.body._id);
      done();
    }
  )
});
Given('un {string} incorrecto para alta sesion', function (string) {
 if(string=="1"){
   var newSession= {
       "email": "dblaslq@elpais.com",
       "password": "a"
   }
   this.session=newSession;
 }else if (string=="2") {
   var newSession= {
       "email": "a",
       "password": "123456"
   }
   this.session=newSession;
 }

});
Then('obtener {string} alta session', function (string,done) {
 var newSession=this.session;
 chai.request('http://localhost:3000')
 .post('/bankjco/v1/sessions')
 .send(newSession)
 .end(
   function(err,res) {
     assert.equal(res.statusCode,string);
     done();
   }
 )
});
Given('un {string} session incorrecto', function (string) {
  this.session=string;
});
Then('obtener un {string} en baja session', function (string,done) {
  var newSession=this.session;
  chai.request('http://localhost:3000')
  .delete('/bankjco/v1/sessions/'+newSession)
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,string);
      done();
    }
  )
});
