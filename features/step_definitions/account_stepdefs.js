const assert = require('assert');
const { Given, When, Then, setDefaultTimeout } = require('cucumber');

const chai = require ('chai');
const chaihttp = require ('chai-http');

setDefaultTimeout(60 * 1000);
chai.use(chaihttp);


Then('Account obtengo una lista', function (done) {
  chai.request('http://localhost:3000')
  .get('/bankjco/v1/users/29/accounts')
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.body.length > 0, true);
      done();
    }
  )
});
Given('Account un {string}, un {string} incorrecto', function (string, string2) {
  this.id=string;
  this.idUser=string2;
});
Then('Account obtener un {string} en detalle', function (string,done) {
  chai.request('http://localhost:3000')
  .get(`/bankjco/v1/users/${this.idUser}/accounts/${this.id}`)
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,string);
      done();
    }
  )
});
Given('Account ids detalle correctos', function () {
  this.id=32;
  this.idUser=29;
});
Then('Account obtener detalle de cuenta', function (done) {
  chai.request('http://localhost:3000')
  .get(`/bankjco/v1/users/${this.idUser}/accounts/${this.id}`)
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,200);
      assert(res.body);
      assert(res.body.IBAN);
      done();
    }
  )
});
