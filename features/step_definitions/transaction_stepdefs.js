const assert = require('assert');
const { Given, When, Then, setDefaultTimeout } = require('cucumber');

const chai = require ('chai');
const chaihttp = require ('chai-http');

setDefaultTimeout(60 * 1000);
chai.use(chaihttp);

Then('Transaction obtengo una lista', function (done) {
  chai.request('http://localhost:3000')
  .get('/bankjco/v1/users/1/accounts/21/transactions')
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.body.length > 0, true);
      done();
    }
  )
});
Given('Transaction un {string}, un {string}, un {string} incorrecto', function (string, string2, string3) {
  this.id=string;
  this.idUser=string2;
  this.idAccount=string3;
});
Then('Transaction obtener un {string} en detalle', function (string,done) {
  chai.request('http://localhost:3000')
  .get(`/bankjco/v1/users/${this.idUser}/accounts/${this.idAccount}/transactions/${this.id}`)
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,string);
      done();
    }
  )
});
Given('Transaction ids detalle correctos', function () {
  this.id=177;
  this.idUser=1;
  this.idAccount=21;
});
Then('Transaction obtener detalle de cuenta', function (done) {
  chai.request('http://localhost:3000')
  .get(`/bankjco/v1/users/${this.idUser}/accounts/${this.idAccount}/transactions/${this.id}`)
  .set("X-tokensec",this.xtoken)
  .end(
    function(err,res) {
      assert.equal(res.statusCode,200);
      assert(res.body);
      assert(res.body.amount);
      assert(res.body.currency);
      done();
    }
  )
});
