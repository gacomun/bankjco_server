@startsalt
{
	Cuenta Alta/Modificacion/Detalle
	..
	{^"Inicio -> Cliente -> Cuenta"
		Concepto| "                 "
		Importe| "9.999 €"
		divisa| ^EUR^
		Tipo|  ^Ingreso^
	}
	{^"Acciones"
		[OK <&share>]| [Cancel <&circle-x>]
	}
}
@endsalt