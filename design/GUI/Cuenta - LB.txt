@startsalt
{
	Cuenta Listado/Borrado
	..
	{^"Busqueda"
		Busqueda | "texto" |  ^Alias^
	}
	{^"Inicio -> Cliente"
		{#
			. | IBAN | Alias | Saldo | Divisa
			(X) | ES0182999999999 | Casa | 15,0 € | EUR
			()  | X | X | X | X
		}
	}
	{^"Acciones"
		||[Cerrar <&share>] | [Movimientos] | [Borrar <&trash>] 
	}
}
@endsalt