@startsalt
{
	Usuario Alta/Modificacion/Detalle
	..
	{^"Inicio"
		Nombre       | "                 "
		Apellido 1   | "                 "
		Apellido 2   | "                 "
		Edad         | "999"
		email        | "                 "
		Password     | "****             "
	}
	{^"Acciones"
		[OK <&share>]| [Cancel <&circle-x>] 
	}
}
@endsalt