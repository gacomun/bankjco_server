@startuml
class Usuario {
	id: Integer
	Nombre: String
	Apellido1: String
	Apellido2: String
	Edad: Integer
	email: String
	pwd: SHASH
	Rol: UserRol
}
class Cuenta {
	id: Integer
	IBAN: String
	alias: String
	saldo: Float
	divisa: String
}
class Movimiento {
	id: Integer
	concepto: String
	importe: Float
	divisa: String
	tipo: MovimientoTipo
}
enum UserRol {
  USUARIO
  ADMINISTRADOR
}
enum MovimientoTipo {
  INGRESO
  ABONO
}
Usuario *- Cuenta 
Cuenta *-- Movimiento 
@enduml