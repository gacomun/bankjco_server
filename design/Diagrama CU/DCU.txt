@startuml
left to right direction
skinparam packageStyle rectangle
actor Usuario
actor Administrador
rectangle Bankjco {
  Usuario --> (Login)
  Usuario --> (Alta\nUsuario)
  Usuario --> (Mant.\nMovimiento)
  Usuario --> (Mant.\nCuenta)
  Usuario --> (Otras\nAPIs)
  (Login) <-- Administrador
  (Login) <..(Mant.\nUsuarios) : include
  (Login) <..(Mant.\nMovimiento) : include
  (Login) <..(Mant.\nCuenta) : include
  (Mant.\nUsuarios) <-- Administrador
  (Alta\nUsuario) <.. (Mant.\nUsuarios) : extends
  (Consulta\nUsuario) <.. (Mant.\nUsuarios) : extends
  (Baja\nUsuario) <.. (Mant.\nUsuarios) : extends
  (Modificacion\nUsuario) <.. (Mant.\nUsuarios) : extends
  (Mant.\nMovimiento) ..> (Alta\nMovimiento) : extends
  (Mant.\nMovimiento) ..> (Baja\nMovimiento) : extends
  (Mant.\nMovimiento) ..> (Modificacion\nMovimiento) : extends
  (Mant.\nMovimiento) ..> (Consulta\nMovimiento) : extends
  (Modificacion\nCuenta) <. (Modificacion\ndivisa) : include
  (Mant.\nCuenta) ..> (Alta\nCuenta) : extends
  (Mant.\nCuenta) ..> (Baja\nCuenta) : extends
  (Mant.\nCuenta) ..> (Modificacion\nCuenta) : extends
  (Mant.\nCuenta) ..> (Consulta\nCuenta) : extends
  (Baja\nMovimiento) <.. (Baja\nCuenta) : extends
  (Baja\nCuenta) <.. (Baja\nUsuario) : extends
}
@enduml